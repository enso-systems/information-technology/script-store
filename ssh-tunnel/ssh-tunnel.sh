#!/usr/bin/env bash

# This script reads the following variables from the environment and starts an
# SSH Tunnel using them:
#
# - REMOTE_HOST
# - REMOTE_PORT
# - LOCAL_HOST
# - LOCAL_PORT
# - JUMPHOST_HOST
# - JUMPHOST_USER
# - JUMPHOST_KEY
#
# Optionally an env file can be passed as a parameter, the script will read its
# contents and use it to start the tunnel. Examples:
#
#   ./ssh-tunnel gcp-database.env
#   ./ssh-tunnel aws-secretserver.env
#
# If DEBUG=True the command will add the -vvv flag for debugging purposes.

#Local variables to be used in the script
FILE=$1
ALL_VARIABLES="true"
KEY="false"

#It is validated that the file that is passed as a parameter exists, if it does not exist, the script will be executed with the environment variables.
#If it exists, the variables of the file are loaded.
if [ ! -f "$FILE" ]; then
    echo ">>> WARINING: The env-file was not passed as a parameter"
    echo "Usage: ssh-tunnel.sh env-file"
    echo "Example: ssh-tunnel.sh env/aws-secretserver.env"
    echo ">>> Trying with environment variables..."
else
    source ${FILE}
fi

#It is validated that all the required variables exist, otherwise a message will be displayed saying which variables are missing to execute the script.
if [ ! $REMOTE_PORT ]; then
    echo ">>> Missing variable REMOTE_PORT"
    ALL_VARIABLES="false"
fi
if [ ! $LOCAL_HOST ]; then
    echo ">>> Missing variable LOCAL_HOST"
    ALL_VARIABLES="false"
fi
if [ ! $LOCAL_PORT ]; then
    echo ">>> Missing variable LOCAL_PORT"
    ALL_VARIABLES="false"
fi
if [ ! $REMOTE_HOST ]; then
    echo ">>> Missing variable REMOTE_HOST"
    ALL_VARIABLES="false"
fi
if [ ! $JUMPHOST_USER ]; then
    echo ">>> Missing variable JUMPHOST_USER"
    ALL_VARIABLES="false"
fi
if [ ! $JUMPHOST_HOST ]; then
    echo ">>> Missing variable JUMPHOST_HOST"
    ALL_VARIABLES="false"
fi
if [ ! $DEBUG ]; then
    echo ">>> Missing variable DEBUG"
    ALL_VARIABLES="false"
fi

#If any variable is missing, the execution of the script stops and an error message is displayed.
if [ $ALL_VARIABLES == "false" ]; then
    echo ">>> Error missing variables"
    exit 1
fi

#It validates that the key file exists.
#If the key file exists but has no content, the script stops.
if [ -f "$JUMPHOST_KEY" ]; then
    if [ ! -s "$JUMPHOST_KEY" ]; then
        echo "$JUMPHOST_KEY" "is empty"
        exit 1
    else
        KEY=="true"

    fi
fi

#If the DEBUG parameter is read as true, the script is executed with -vvv, otherwise without -vvv.
if [ $DEBUG = "true" ]; then
    #If the parameter JUMPHOST_KEY exists and is valid, the command will be executed with -i ${JUMPHOST_KEY}, otherwise without -i ${JUMPHOST_KEY}
    if [ $KEY = "true" ]; then
        echo ">>> This command will be executed"
        echo "	ssh -vvv \
				-i ${JUMPHOST_KEY} \
				-L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
				-N ${JUMPHOST_USER}@${JUMPHOST_HOST}"

        ssh -vvv \
            -i ${JUMPHOST_KEY} \
            -L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
            -N ${JUMPHOST_USER}@${JUMPHOST_HOST}
    else
        echo ">>> This command will be executed"
        echo "	ssh -vvv \
				-L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
				-N ${JUMPHOST_USER}@${JUMPHOST_HOST}"

        ssh -vvv \
            -L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
            -N ${JUMPHOST_USER}@${JUMPHOST_HOST}
    fi
else
    #If the parameter JUMPHOST_KEY exists and is valid, the command will be executed with -i ${JUMPHOST_KEY}, otherwise without -i ${JUMPHOST_KEY}
    if [ $KEY = "true" ]; then
        echo ">>> This command will be executed"
        echo "ssh -i ${JUMPHOST_KEY} \
		-L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
		-N ${JUMPHOST_USER}@${JUMPHOST_HOST}"

        ssh -i ${JUMPHOST_KEY} \
            -L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
            -N ${JUMPHOST_USER}@${JUMPHOST_HOST}
    else
        echo ">>> This command will be executed"
        echo "ssh -L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
		-N ${JUMPHOST_USER}@${JUMPHOST_HOST}"

        ssh -L ${LOCAL_HOST}:${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
            -N ${JUMPHOST_USER}@${JUMPHOST_HOST}
    fi
fi
